*** Settings ***
Library     SeleniumLibrary
Resource    exercice9.resource

*** Test Cases ***
BDD_Case_01_HealthcareProgram
    [Setup]       Test Setup
    Given Je suis connecté sur la page de prise de rendez-vous
    When Je renseigne les informations obligatoires                         ${Jdd1.facility}
    And Je choisis Healthcare Program                                       ${Jdd1.healthcareProgram}
    And Je clique sur Book Appointment
    Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd1.verifHealthcareProgram}    ${Jdd1.facility}
    [Teardown]    Test Teardown

BDD_Case_02_HealthcareProgram
    [Setup]       Test Setup
    Given Je suis connecté sur la page de prise de rendez-vous
    When Je renseigne les informations obligatoires                         ${Jdd2.facility}
    And Je choisis Healthcare Program                                       ${Jdd2.healthcareProgram}
    And Je clique sur Book Appointment
    Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd2.verifHealthcareProgram}    ${Jdd2.facility}
    [Teardown]    Test Teardown

BDD_Case_03_HealthcareProgram
    [Setup]       Test Setup
    Given Je suis connecté sur la page de prise de rendez-vous
    When Je renseigne les informations obligatoires                         ${Jdd3.facility}
    And Je choisis Healthcare Program                                       ${Jdd3.healthcareProgram}
    And Je clique sur Book Appointment
    Then Le rendez-vous est confirmé et le HealthcareProgram est affiché    ${Jdd3.verifHealthcareProgram}    ${Jdd3.facility}
    [Teardown]    Test Teardown
